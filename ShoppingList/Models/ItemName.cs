﻿using ShoppingList.Resources.Languages;
using System.ComponentModel.DataAnnotations;

namespace ShoppingList.Models
{
    public class ItemName
    {
        [Required(ErrorMessageResourceName = "NameIsRequired", ErrorMessageResourceType = typeof(Translations))]
        public string Name { get; set; }
    }
}
