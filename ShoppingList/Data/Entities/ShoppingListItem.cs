﻿namespace ShoppingList.Data.Entities
{
    public class ShoppingListItem
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public bool IsBought { get; set; }
    }
}
