﻿using ShoppingList.Data.Entities;
using ShoppingList.Services;
using ShoppingList.ViewModels;

namespace ShoppingList.Data.Repositories
{
    public interface IShoppingListRepository
    {
        Task AddNewShoppingList(Entities.ShoppingList newShoppingList);
        Task<List<BaseViewModel>> GetAllShoppingLists();
        Task AddListItem(Guid shoppingListId, ShoppingListItem shoppingListItem);
        Task<Entities.ShoppingList> GetShoppingList(Guid shoppingListId);
        Task RemoveListItem(Guid shoppingListId, Guid shoppingListItemId);
        Task RemoveShoppingList(Guid shoppingListId);
        Task UpdateListItem(Guid shoppingListId, Guid shoppingListItemId, bool isBought);
    }

    public class ShoppingListRepository : BaseRepository, IShoppingListRepository
    {
        private const string shoppingListCollectionName = "shoppingList";

        public ShoppingListRepository(ISecureStorageService secureStorageService) : base(secureStorageService)
        {
        }

        public async Task AddNewShoppingList(Entities.ShoppingList newShoppingList)
        {
            var shoppingListCollection = await Init<Entities.ShoppingList>(shoppingListCollectionName);

            shoppingListCollection.Insert(newShoppingList);

            shoppingListCollection.EnsureIndex(x => x.Id);
        }

        public async Task<List<BaseViewModel>> GetAllShoppingLists()
        {
            var shoppingListCollection = await Init<Entities.ShoppingList>(shoppingListCollectionName);

            return shoppingListCollection.Query()
                .Select(s => new BaseViewModel { Id = s.Id, Name = s.Name })
                .ToEnumerable()
                .OrderBy(s => s.Name)
                .ToList();
        }

        public async Task RemoveShoppingList(Guid shoppingListId)
        {
            var shoppingListCollection = await Init<Entities.ShoppingList>(shoppingListCollectionName);
            var shoppingList = shoppingListCollection.Query().Where(s => s.Id == shoppingListId).ToDocuments().FirstOrDefault();

            if (shoppingList != null)
            {
                shoppingListCollection.Delete(shoppingList["_id"]);
            }
        }

        public async Task AddListItem(Guid shoppingListId, ShoppingListItem shoppingListItem)
        {
            var shoppingListCollection = await Init<Entities.ShoppingList>(shoppingListCollectionName);
            var shoppingList = shoppingListCollection.Query().Where(s => s.Id == shoppingListId).FirstOrDefault();
            shoppingList.Items.Add(shoppingListItem);

            shoppingListCollection.Update(shoppingList);
        }

        public async Task<Entities.ShoppingList> GetShoppingList(Guid shoppingListId)
        {
            var shoppingListCollection = await Init<Entities.ShoppingList>(shoppingListCollectionName);
            return shoppingListCollection.Query().Where(s => s.Id == shoppingListId).FirstOrDefault();
        }

        public async Task RemoveListItem(Guid shoppingListId, Guid shoppingListItemId)
        {
            var shoppingListCollection = await Init<Entities.ShoppingList>(shoppingListCollectionName);
            var shoppingList = shoppingListCollection.Query().Where(s => s.Id == shoppingListId).FirstOrDefault();
            var listItem = shoppingList.Items.Find(s => s.Id == shoppingListItemId);

            if (listItem != null)
            {
                shoppingList.Items.Remove(listItem);

                shoppingListCollection.Update(shoppingList);
            }
        }

        public async Task UpdateListItem(Guid shoppingListId, Guid shoppingListItemId, bool isBought)
        {
            var shoppingListCollection = await Init<Entities.ShoppingList>(shoppingListCollectionName);
            var shoppingList = shoppingListCollection.Query().Where(s => s.Id == shoppingListId).FirstOrDefault();
            var listItem = shoppingList.Items.Find(s => s.Id == shoppingListItemId);

            if (listItem != null)
            {
                listItem.IsBought = isBought;

                shoppingListCollection.Update(shoppingList);
            }
        }
    }
}
