﻿using LiteDB;
using ShoppingList.Services;

namespace ShoppingList.Data.Repositories
{
    public abstract class BaseRepository
    {
        private readonly ISecureStorageService secureStorageService;
        private LiteDatabase _db;

        protected BaseRepository(ISecureStorageService secureStorageService)
        {
            this.secureStorageService = secureStorageService;
        }

        private async Task<LiteDatabase> InitDatabase()
        {
            if (_db != null)
                return _db;

            _db = new LiteDatabase(await GetConnectionString());

            return _db;
        }

        protected async Task<ILiteCollection<T>> Init<T>(string collectionName)
        {
            var db = await InitDatabase();

            return db.GetCollection<T>(collectionName);
        }

        protected async Task<string> GetConnectionString()
        {
            var path = FileSystem.Current.AppDataDirectory;

            var fullPath = Path.Combine(path, "shopping-list.db");

            var password = await secureStorageService.Get("password");

            if (password == null)
            {
                password = Guid.NewGuid().ToString();
                await secureStorageService.Save("password", password);
            }

            var connectionString = $"Filename={fullPath};Password={password}";


            return connectionString;
        }

        ~BaseRepository()
        {
            _db.Dispose();
        }
    }

}
