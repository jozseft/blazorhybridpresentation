﻿namespace ShoppingList.ViewModels
{
    public class BaseViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
