﻿namespace ShoppingList.Services
{
    public interface IShareService
    {
        Task ShareText(string title, string text);
    }

    public class ShareService : IShareService
    {
        public async Task ShareText(string title, string text)
        {
            await Share.Default.RequestAsync(new ShareTextRequest
            {
                Title = title,
                Text = text
            });
        }
    }
}
